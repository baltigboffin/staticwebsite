Create clone git repository to local computer.

```bash
$ git clone  https://gitlab.com/baltigboffin/mywebsiteproject.git
```

After the project has been downloaded you can make changes to the files locally.  Then add the file that has been changed, commit it, and push it back to GitLab.

```bash
$ git add *.html
$ git commit -m "changed version 2 to 3"
$ git push
```

Any changes made on the GitLab website can be pulled down to the local copy 

```bash
$ git pull
```