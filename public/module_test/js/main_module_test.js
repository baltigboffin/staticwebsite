console.log( "A + begin" );

globalThis.tracker = globalThis.tracker + ['A'];

import {m4} from './app/mod4.js'

export var mta = 'mta';

export var m4imported = 'mta ' + m4;

console.log( "A - globalThis.tracker:", globalThis.tracker );  // this is undefined because main_mod executes before globalThis.tracker is defined in the html file.

console.log( "A -", m4 );

console.log( "A + end" );

