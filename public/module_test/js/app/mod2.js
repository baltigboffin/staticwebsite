console.log( "2 + begin" );

globalThis.tracker = globalThis.tracker + ['2'];

console.log( "2 - globalThis.tracker:", globalThis.tracker );  // this works because mod2 executes after html folder
console.log( "2 - window.tracker:",     window.tracker     );  // this also works


export var m2 = "m2";

console.log( "2 + end" );
