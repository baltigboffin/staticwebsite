var re_newline = /\n/g

function processCode() {

  var theNodes = d3.selectAll('body .code')._groups[0];
  var codeSnippets = {};

  var numberOfNewlines = 0;
  var lineNumbers = "";

  theNodes.forEach( (d) => {
    codeSnippets[d.id] = d.innerHTML;
  } );

  var theBody = d3.select('body');

  Object.keys( codeSnippets ).forEach( (d) => {
    
    numberOfNewlines = codeSnippets[d].match(re_newline).length;
    lineNumbers = ""; 

    divWrapper = d3.select( ("#"+d) );
    divWrapper
      .classed("code", false)
      .classed("wrapper", true)
      .html("")

    for( let i = 0; i < numberOfNewlines; i++ ) {
      lineNumbers += (i+1) + "\n";
    }

    divNumbers = divWrapper
      .append('pre')
      .classed('lineNumbers', true)
      .html( lineNumbers );

    divLines = divWrapper
      .append('pre')
      .classed('snippets', true)
      .html( codeSnippets[d] );

  });
 
}
