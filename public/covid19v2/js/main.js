import { getData } from './app/data.js';
import './lib/d3.v5.min.js';
import './lib/d3-scale.v3.min.js';

const showNegativeTrend = false;

var mainFields = [];            // List of non date fields
var keyFields  = [];            // List of date fields
const beginDate = "01/10/2020"; // "02/10/2020";
const endDate   = "06/30/2020";
var newData    = [];
var stackData  = undefined;
var locations = [];
var locationDailyDelta = {};
var locationLastWeekChange = [];
var location2ndLastWeekChange = [];
var maxValue  = -1;
var maxDailyDelta = -1;
var locationMaxDailyDelta = -1;
var keepLocations = [];
var keepLocationsHash = {};
const height =  800;
const width  = 1350;
var columnWidth = -1;       // This is calculated later depending on the number of columns displayed and the width.
var xAxisFontSize = "6px";  //"12px" // "6px"  // This is calculated later depending on the number of columns displayed and the width.
var filter_locationMaxDailyDelta = 0; 
var scaleY      = undefined;
var scaleY2     = undefined;
var color       = undefined;
var gYAxisR     = undefined;
var yAxisRDates = undefined;
var yAxisRTotal = undefined;
var colorRelativeToCountry = undefined;
var colorScaleRelativeToCountry = undefined;

var inputChartType       = undefined;
var inputDataSource      = undefined;
var inputColorRelativeTo = undefined;

console.log("main.js begin");

var stateAcronyms = {
"Alabama":"AL",
"Alaska":"AK",
"Arizona":"AZ",
"Arkansas":"AR",
"California":"CA",
"Colorado":"CO",
"Connecticut":"CT",
"Delaware":"DE",
"Florida":"FL",
"Georgia":"GA",
"Hawaii":"HI",
"Idaho":"ID",
"Illinois":"IL",
"Indiana":"IN",
"Iowa":"IA",
"Kansas":"KS",
"Kentucky":"KY",
"Louisiana":"LA",
"Maine":"ME",
"Maryland":"MD",
"Massachusetts":"MA",
"Michigan":"MI",
"Minnesota":"MN",
"Mississippi":"MS",
"Missouri":"MO",
"Montana":"MT",
"Nebraska":"NE",
"Nevada":"NV",
"New Hampshire":"NH",
"New Jersey":"NJ",
"New Mexico":"NM",
"New York":"NY",
"North Carolina":"NC",
"North Dakota":"ND",
"Ohio":"OH",
"Oklahoma":"OK",
"Oregon":"OR",
"Pennsylvania":"PA",
"Rhode Island":"RI",
"South Carolina":"SC",
"South Dakota":"SD",
"Tennessee":"TN",
"Texas":"TX",
"Utah":"UT",
"Vermont":"VT",
"Virginia":"VA",
"Washington":"WA",
"West Virginia":"WV",
"Wisconsin":"WI",
"Wyoming":"WY",
"District of Columbia":"DC",
"Marshall Islands":"MH" }

var global_recovered = {
  title:      "Global Recovered",
  pageheader: 'Covid 19 data updated daily from <a href="https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series">019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE</a><br><i>time_series_covid19_recovered_global.csv</i>', 
  url:        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv",
  majorLoc:   "Country/Region",
  minorLoc:   "Province/State",
  filter_locationMaxDailyDelta: 1000
}

var global_death = {
  title:      "Global Deaths",
  pageheader: 'Covid 19 data updated daily from <a href="https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series">019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE</a><br><i>time_series_covid19_deaths_global.csv</i>', 
  url:        "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv",
  majorLoc:   "Country/Region",
  minorLoc:   "Province/State",
  filter_locationMaxDailyDelta: 100
}

var us_death = {
  title:      "U.S. Deaths",
  pageheader: 'Covid 19 data updated daily from <a href="https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series">019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE</a><br><i>time_series_covid19_deaths_US.csv</i>', 
  url:      "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_US.csv",
  majorLoc: "Province_State",
  minorLoc: "Admin2", 
  filter_locationMaxDailyDelta: 50
}

var global_confirmed = {
  title:      "Gloabl Confirmed Cases",
  pageheader: 'Covid 19 data updated daily from <a href="https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series">019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE</a><br><i>time_series_covid19_confirmed_global.csv</i>', 
  url:      "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv",
  majorLoc: "Country/Region",
  minorLoc: "Province/State",
  filter_locationMaxDailyDelta: 1000
}

var us_confirmed = {
  title:      "U.S. Confirmed Cases",
  pageheader: 'Covid 19 data updated daily from <a href="https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series">019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE</a><br><i>time_series_covid19_confirmed_US.csv</i>', 
  url:      "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv",
  majorLoc: "Province_State",
  minorLoc: "Admin2",
  filter_locationMaxDailyDelta: 800
}

var changeGlobalVariables = function( url ) {
  d3.select( "title"       ).html( url.title      );
  d3.select( "#pageheader" ).html( url.pageheader );
  
  filter_locationMaxDailyDelta = url.filter_locationMaxDailyDelta;
  console.log( "filter_locationMaxDailyDelta", filter_locationMaxDailyDelta);
}

var mainPageSetup = function( url ) {

// This function sets up the main page features that are not reliant on the imported data set

  changeGlobalVariables( url );

  inputChartType = d3.select( "#menu"   )
    .append("select")
    .attr("name","chartType")
    .attr("value", "NO VALUE!")
    .classed("button", true)
    .attr("id","chartType")
    .on( "change", change );

  inputChartType.selectAll("option")
    .data(["Stacked Height by Day","Stacked Height by Total"])
    .enter()
    .append("option").attr("value", (d)=>{return d;} ).html( (d)=>{return d;} );

  inputChartType.property("selectedIndex", 1);

// The code below creates the combo box for selecting the data source
/*
  inputDataSource = d3.select( "#menu"    )
    .append("select")
    .attr("name","dataSource")
    .attr("value", "NO VALUE!")
    .classed("button", true)
    .attr("id","dataSource");

  inputDataSource.selectAll("option")
    .data(["Global Death","US Death","Global Confirmed","US Confirmed","Global Recovered"])
    .enter()
    .append("option").attr("value", (d)=>{return d;} ).html( (d)=>{return d;} );

  inputDataSource.propterty("selectedIndex", 1);
*/

  inputColorRelativeTo = d3.select( "#menu" )
    .append("select")
    .attr("name","colorRelativeTo")
    .attr("value", "No VALUE!")
    .classed("button", true)
    .attr("id", "colorRelativeTo")
    .on( "change", change );

  inputColorRelativeTo.selectAll("option")
    .data(["Color Relative To Chart","Color Relative To Column"])
    .enter()
    .append("option").attr("value", (d)=>{return d;} ).html( (d)=>{return d;} );

  inputColorRelativeTo.property("selectedIndex", 0);

}

var dataReceived = function( x, template ) {

//  Create list of fields that are not dates called mainFields
  (x.meta.fields).forEach( (d) => {
    if( (new Date(d)).toString() === "Invalid Date") {
      mainFields.push( d);
    }
  });

  keyFields = (x.meta.fields);
  
//  Remove non-date fields 

  (mainFields).forEach( (a) => {
    let i = (keyFields).indexOf( a );
    (keyFields).splice(i,1);
  });
  
//  Identify and Remove keyFields outside of date range
  let deleteKeys = [];
  for( let i = keyFields.length; i >= 0; i-- ) {
    let date = keyFields[i];
    if( (date < beginDate) || (date > endDate) ) {
      deleteKeys.push( date );
      (keyFields).splice(i, 1);
    }
  }
//  Remove data outside of date range
  (x.data).forEach( (row) => {
    (deleteKeys).forEach( (date) => {
      delete row.date;
    });
  });
  console.log("Result: ", x );

//  createNewData( x );



// processData should filter out dates and fields that are relevant to the graph prior to creating the stack data
  stackData = processData( x, template );
  evaluateStackData( stackData );
  ux( stackData );
}

var createNewData = function( result ) {
  // HERE WE ARE GOING TO ADD DATA TO A NEW DATA STRUCTURE WITH CONSOLIDATION ON THE STATE OR COUNTRY FIELD
  // AND WE ARE GOING TO FILL IN THE GAPS FOR MISSING OR NEGATIVE DATA.
  (result.data).forEach( (d) => {
    
  });
}

var createLocation = function( country, state ) {
  let answer = "";

//  Remove "Unassigned" check?  This is taken care of in the data module.
  if( (state != "" ) && (state != "Unassigned"  )) {
    if( stateAcronyms[country] != undefined ) {
      answer = state + ", " + stateAcronyms[country] ;
    } else {
      answer = state + ", " + country ;
    }
  } else {
    answer = country ;
  }

  return answer;
}

var processData = function( result, url ) {


//  Create "Locations" field in data and create the locations array
  (result.data).forEach( ( row ) => {
    row["Location"] =  createLocation( row[ url.majorLoc ], row[ url.minorLoc ] );
    locations.push( row.Location );
  });

//  Format the data for stack chart
  let stack = d3.stack()
    .keys( keyFields )
    .value( (d,key)=> {
      let answer = 0;
// Calculate the value for that date by subtracting the previous date from the current date.
// If the key (aka date) is not the first day find the difference in values between it and the previous date.
// If it is the first date in the table use that date without subtracting a previous date's value.
      let j = keyFields.indexOf(key) - 1;  
      if( j > -1  ) { answer = d[key] - d[ keyFields[ j ]]; }
      else { answer = d[key]; }

      return answer;
    });

  let series = stack( result.data );

  return series;
}

var evaluateStackData = function( series ) {

  locationDailyDelta = {};
  maxValue  = -1;
  maxDailyDelta = -1;
  locationMaxDailyDelta = -1;

// Look at the last key (aka date) in the series and find the max value 
// This assumes the data is cumulative and the last value will be the largest.
  (series[ series.length - 1]).forEach( (loc) => {
    if( loc[1] > maxValue ) { maxValue = loc[1]; } 
  });

// Create empty locationDailyDelta hash table to store the delta of every location/day
  locations.forEach( (loc) => {
    locationLastWeekChange[ loc ] = 0;
    location2ndLastWeekChange[ loc ] = 0;
    locationDailyDelta[ loc ] = new Object();
    keyFields.forEach( (date) => {
      locationDailyDelta[ loc ][ date ] = undefined;
    });
  });

  let location = undefined;

//  Fill locationDailyDelta hash table with delta values from the series
  (series).forEach( (date) => {
    date.forEach( (loc) => {
      location = loc.data["Location"];
      locationDailyDelta[ location ][ date.key ] = loc[1] - loc[0];
    });
  });

  console.log( "series[ series.length - 1]", series[ series.length - 1]);
  
  for( let i = series.length - 1; i >= series.length - 14; i-- ) {

    (series[i]).forEach( (loc) => {
      let location = loc.data["Location"];
      locationLastWeekChange[location] += loc[1] - loc[0];
    })
    let j = i - 14;
    (series[j]).forEach( (loc) => {
      let location = loc.data["Location"];
      location2ndLastWeekChange[location] += loc[1] - loc[0];
    })

  }

  (Object.keys(locationLastWeekChange)).forEach( (loc) => {
    locationLastWeekChange[ loc ] = ( locationLastWeekChange[ loc ] - location2ndLastWeekChange[ loc ] );
  })

  console.log( "locationLastWeekChange", locationLastWeekChange );
  console.log( "New York, NY", typeof( locationLastWeekChange[ "New York, NY" ] ) )

//  Create and fill locationMaxDailyDelta to show the largest delta for each location.
  locationMaxDailyDelta =
    (Object.keys(locationDailyDelta).reduce( 
      (acc,x) => {
        acc[x]=Math.max.apply(Math, Object.values(locationDailyDelta[x]));
        return acc},{}
    ))


//  Identify the highest delta for all locations
  maxDailyDelta = Math.max.apply( Math, Object.values( locationMaxDailyDelta ) );

//  Create keepLocations array with location name and index 

if( showNegativeTrend == false ) {
  (Object.keys(locationMaxDailyDelta)).forEach( (loc, i) => {
    if( locationMaxDailyDelta[loc] > filter_locationMaxDailyDelta ) {
      keepLocations.push( {name: loc, index:i} );
    }
  });
} else {
  (Object.keys(locationLastWeekChange)).forEach( (loc, i) => {
    if( locationLastWeekChange[loc] > 500 ) {      // 1000 for global_confirmed  // 500 for us confirmed
      keepLocations.push( {name: loc, index:i} );
    }
  });
}

//  Calculate the column width, but limit the width to 40px
//  AdjForRightAxis is a value to provide space for the axis on the right side of the chart.
  let AdjForRightAxis = 60;
  columnWidth   = Math.trunc( (width - AdjForRightAxis)/ keepLocations.length );
  columnWidth   = ( columnWidth > 40 ? 40 : columnWidth );

//  Calculate the font size, but limit it to between 6px and 12px
  let tempFontSize = (columnWidth/6) + 5;
  xAxisFontSize = "" + ( tempFontSize > 10 ? 10 : tempFontSize) + "px";
}

var filterSeries = function( location ) {
  let answer = false;
  
  if(( Object.keys( keepLocationsHash ).length < 1 ) && ( keepLocations.length > 0 )){
    keepLocations.forEach( (l) => { keepLocationsHash[l.name] = l.index });
  }

  if( keepLocationsHash[ location ] > -1 ) {  answer = true;}

  return answer;
}

var ux = function( series ) {

  var dateDomain   = [ new Date( keyFields[0] ), new Date( keyFields[ keyFields.length-1 ] ) ];

//  The heightDomain -1 to 0 handles data indicating total deaths were negative
  var heightDomain =  [ -1, 0, (maxDailyDelta/10), maxDailyDelta ];

  colorRelativeToCountry = function( locName, delta ) {

    if( colorScaleRelativeToCountry === undefined ) {
      colorScaleRelativeToCountry = {};
      keepLocations.forEach( (l) => {
        let locDailyDeltaMax = locationMaxDailyDelta[ l.name ]
        colorScaleRelativeToCountry[ l.name ] = d3.scaleLinear()
          .domain( [ -1, 0, (locDailyDeltaMax/10), locDailyDeltaMax ] )
          .range(  [d3.rgb("#00FF00"), d3.rgb("#FFFFFF"),  d3.rgb("#8888FF"), d3.rgb("#FF0000")])
      });
    }

    let answer =  (colorScaleRelativeToCountry[ locName ])( delta )
    return answer;
  }

  color = d3.scaleLinear()
    .domain( heightDomain )
    .range([ d3.rgb("#00FF00"), d3.rgb("#FFFFFF"),  d3.rgb("#8888FF"), d3.rgb("#FF0000")])

  var rangeValue = [];
  for( let i = 0; i < keepLocations.length; i++ ) {
    rangeValue[i] = i*columnWidth;
  }

  var scaleX = d3.scaleOrdinal()
    .domain( locations.filter( (d) => { return filterSeries( d ); } ) )
    .range( rangeValue )

  scaleY = d3.scaleLinear()
    .domain([0,maxValue])
    .range( [ 0, height-130]);

  var scaleYReverse = d3.scaleLinear()
    .domain([maxValue, 0])
    .range( [ 0, height-130]);

  let arrOfKeyFieldsAsDates

  scaleY2 = d3.scaleLinear()
//    .domain([ keyFields[0], keyFields[keyFields.length-1] ])
    .domain([ new Date( keyFields[0] ), new Date( keyFields[keyFields.length-1]) ])
    .range( [ 0, height-130]);

  var xAxisT = d3.axisTop(scaleX)
    .tickValues( locations.filter( (d) => { return filterSeries( d ); } ) );

  var xAxisB = d3.axisBottom(scaleX)
    .tickValues( locations.filter( (d) => { return filterSeries( d ); } ) );

  let temp = keyFields.map( (d) => {return d.toString(); });

  let scaleY3 = d3.scaleLinear()
//    .domain([ keyFields[ keyFields.length - 1 ], keyFields[ 0] ])
    .domain([ new Date( keyFields[ keyFields.length - 1 ]), new Date( keyFields[ 0]) ])
    .range( [ 0, height-130 ]);

  let tempKeyFields = [];
  let tickStep = Math.trunc( keyFields.length / 40);  // with 87 dates the value will be ~2 so every other date will be displayed.
  keyFields.forEach( (a,i) => { if( i % tickStep === 0 ) { tempKeyFields.push( new Date( a ) );} } );

  yAxisRDates = d3.axisRight(scaleY3)
    .tickValues( tempKeyFields  )
    .tickFormat( d3.timeFormat( "%m/%d/%Y") )

  let s = Math.round( maxValue / 40 );
  let tempArray = [];
  for( let i=maxValue; i > 0; i -= s ) {
    tempArray.push( i );
  }

  yAxisRTotal = d3.axisRight( scaleYReverse )
    .tickValues( tempArray  )

  var svg = d3.select("body")
    .append("svg")
    .attr("width",  width )
    .attr("height", height+100 );

// Add date to location level of the stack
  series.forEach( (date) => {
    date.forEach( (loc)  => { 
      loc.push( date.key );
      loc.push( loc.data.Location );
    });
  });

  svg.selectAll("g")
    .enter()
    .data( series )
    .enter().append("g")
      .selectAll("rect")
      .data( (d,i) => {  return d; })
      .enter()
      .filter( (d) => { if( d[3] === "_New York, New York" ){ console.log(d);} return filterSeries( d[3] ); } )
      .append("rect")
//      .attr("stroke-width", (d)  => { return 0;                                                 })
//      .attr("stroke", (d)        => { return ( (d[1]-d[0]) > 0 ? "#EEE" : "#FFF")               })
      .attr("x", (d,i)           => { return i*columnWidth                                      })
      .attr("width" , (d)        => { return columnWidth;                                       })
      .attr("y", (d)             => { return (height - 30 - scaleY(d[0]) - scaleY(d[1]-d[0]));  })
      .attr("height", (d)        => { return scaleY(d[1] - d[0]);                               })
      .attr("fill", (d)          => { let j = inputColorRelativeTo.property('selectedIndex');
                                      return ( j === 0 ? color(d[1]-d[0]) : colorRelativeToCountry( d[3], d[1]-d[0] ) ); })
      .on("mouseover",(d)        => { hover( d );                             });

  

  svg.append("g")
    .attr("transform","translate(" + (columnWidth/2) + ",780)")
    .classed("xAxisB", true)
    .call(xAxisB);

  svg.append("g")
    .attr("transform","translate(" + (columnWidth/2) + ",100)")
    .classed("xAxisT", true)
    .call(xAxisT);

  gYAxisR = svg.append("g")
    .attr(   "transform"  , "translate(" + columnWidth * (Object.keys( keepLocationsHash ).length) + ", 100)" )
    .classed("yAxisR", true)
    .call(yAxisRTotal);

  svg.select(".xAxisT").selectAll("text")
    .style(  "font-size"  , xAxisFontSize )
    .style(  "text-anchor", "end")
    .attr(   "transform"  , "translate(-10,-10) rotate(90)" );

  svg.select(".xAxisB").selectAll("text")
    .style(  "font-size"  , xAxisFontSize )
    .style(  "text-anchor", "start")
    .attr(   "transform"  , "translate(11,7) rotate(90)" );

}

var hover  = function( d ) {
  let answer = d[3];
  let a = d[2].split("/");
//  answer  = d[3] + "&nbsp;&nbsp;" + (d[2].getMonth()+1) + "/" + (d[2].getDate()) + "/" + (d[2].getYear()) +
//            "<br>Daily Change: " + (d[1] - d[0]).toString() + 
//            "<br>Total: " + d[1]  ;
  answer  = d[3] + "&nbsp;&nbsp;" + a[0] + "/" + a[1] + "/" + a[2] +
            "<br>Daily Change: " + (d[1] - d[0]).toString() + 
            "<br>Total: " + d[1]  ;

  d3.select("#popup")
    .style("border","1px solid #666")
    .style("background-color", () => { let j = inputColorRelativeTo.property('selectedIndex');
                                      return ( j === 0 ? color(d[1]-d[0]) : colorRelativeToCountry( d[3], d[1]-d[0] ) ); })
    .html( answer );

}

var change = function() {

  let i = inputChartType.property('selectedIndex');
  let j = inputColorRelativeTo.property('selectedIndex');

  let t = d3.transition()
    .duration(1500)

  switch( i ) {
    case 1:
      d3.selectAll("rect").transition(t)
        .attr("y", (d)             => { return (height - 30 - scaleY(d[0]) - scaleY(d[1]-d[0]));  })
        .attr("height", (d)        => { return scaleY(d[1] - d[0]);                               })
        .style("fill", (d)         => { return ( j === 0 ? color(d[1]-d[0]) : colorRelativeToCountry( d[3], d[1]-d[0] ) ); });

      gYAxisR
        .call(yAxisRTotal);      

    break;
      case 0:
      let dailyHeight = Math.trunc( (height-30)/(keyFields.length-1) )

      d3.selectAll("rect").transition(t)
//        .attr("y",      (d)       => { return (height - 30 - scaleY2( d[2] )); } )
        .attr("y",      (d)       => { return (height - 30 - scaleY2( new Date( d[2] ) )); } )
        .attr("height", (d)       => { return dailyHeight; })
        .style("fill",  (d)       => { return ( j === 0 ? color(d[1]-d[0]) : colorRelativeToCountry( d[3], d[1]-d[0] ) ); });

      gYAxisR
        .call(yAxisRDates);

      break;
    default:
      console.log( "in default" );
  }
}
  mainPageSetup( global_confirmed );

//  getData( us_confirmed, dataReceived  );
//  getData( global_death, dataReceived  );
//  getData( us_death, dataReceived  );
  getData( global_confirmed, dataReceived  );

console.log("main.js end")
