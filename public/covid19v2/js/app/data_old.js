export class Data{
  
  stats = null;
  keys  = null;
  dates = null;

  constructor( message ) {
    console.log( message );
  }

  callbackComplete( dataObject, results, callback ) {

    dataObject.stats = results;
    dataObject.keys  = results.data.shift();
    dataObject.dates = (dataObject.keys.slice(4)).map( (x) => {
                                                      return (new Date(x));
                                                    });
//    dataObject.dates = dataObject.keys.slice(4);

    callback( results.data );

    return true;
  }

  organize( ) {

    console.log( "organize begin" );

    let answer = 
      (this.stats.data).map( (x) => {
        let obj = {};
        obj[ this.keys[0] ] = x.shift();
//        obj[ this.keys[1] ] = x.shift();
        obj[ this.keys[1] ] = (obj[ this.keys[0] ].length == 0 ? x.shift() : x.shift() + ", " + obj[this.keys[0]]);
        obj[ this.keys[2] ] = x.shift();
        obj[ this.keys[3] ] = x.shift();
//        obj[ "Total Deaths" ] = x;
/*
        obj[ "Total Deaths" ] = x.map( (a, i) => {
                                  let obj2 = {};
                                  obj2[ this.dates[i] ] = a;
                                  return obj2;
                                })
*/
        let deltaDeaths = [];
        for( let i = 0; i < x.length; i++) {
          deltaDeaths[i] = ((i<1) ? x[i] : x[i]-x[i-1]);
        }
        
        let obj2 = x.reduce( (acc, a, i) => {
                                    acc[ new Date(this.dates[i]) ] = a;
                                    return acc;
                                  }, {} );
        obj[ "Total Deaths" ] = obj2;

        obj2 = deltaDeaths.reduce( (acc, a, i) => {
                                    acc[ new Date(this.dates[i]) ] = a;
                                    return acc;
                                  }, {} );
        obj[ "Daily Deaths" ] = obj2;

        return obj;  
      });

    answer.pop();

    console.log( "organize end" );

    return answer;
  }

  getData( url, callback ) {

    var aCallback = this.callbackComplete;
    var dataObject = this;

    Papa.parse(url, {
      download: true,
      complete: function( results ) {
        aCallback( dataObject, results, callback );
      }
    });

    console.log("data.js end");
    
  }
              
}

