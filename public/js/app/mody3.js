console.log( "mody3.js" );

var plusOne = function( x ) {
  return (x + 1);
}

var plusTwo = function( x ) {
  return (x + 2);
}

var plusThree = function( x ) {
  return (x + 3);
}

export {plusOne, plusTwo, plusThree};
