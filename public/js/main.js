console.log( "main.js" );

import {apple} from './app/mody1.js';
import {addTwo} from './app/mody1.js';
import {multTwo, multThree} from './app/mody2.js';
import * as m3 from './app/mody3.js';
import * as littles from './app/littles.js';

const resolvedProm = Promise.resolve(3);

console.log( apple );

console.log( "mody1 - addTwo - ", addTwo( 10, 6 ) );

console.log( "mody2 - multTwo - ", multTwo( 10, 6 ) );

console.log( "mody2 - multThree - ", multThree( 2, 3, 4 ) );

console.log( "m3 - plusOne - ", m3.plusOne( 0 ) );
console.log( "m3 - plusTwo - ", m3.plusTwo( 0 ) );
console.log( "m3 - plusThree - ", m3.plusThree( 0 ) );

console.log( "l1 - ", littles.lmod1(3) );
console.log( "l2 - ", littles.lmod2(3) );
console.log( "l3 - ", littles.lmod3(3) );
