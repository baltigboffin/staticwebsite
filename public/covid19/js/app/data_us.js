export class Data{
  
  stats = null;
  keys  = null;
  dates = null;

  constructor( message ) {
    console.log( message );
  }

  callbackComplete( dataObject, results, callback ) {

    dataObject.stats = results;
    dataObject.keys  = results.data.shift();
    dataObject.dates = (dataObject.keys.slice(12)).map( (x) => {
                                                      return (new Date(x));
                                                    });
    dataObject.states = null;
//    dataObject.dates = dataObject.keys.slice(4);

    callback( results.data );

    return true;
  }

  summarize( ) {
    let answer = {};

    answer = (this.stats).reduce( (acc, a, i) => {
      if( a.Province_State != undefined ) {
//                                          console.log( "a.Province_State: ", a.Province_State );
//                                          console.log( "acc: ", acc );
                                          if( acc[ a.Province_State ] == null ) {
                                            acc[ a.Province_State ] = {};
                                            acc[ a.Province_State ].Province_State  = a.Province_State;
                                            acc[ a.Province_State ].Population      = Number( a.Population );
                                            acc[ a.Province_State ]["Total Deaths"] = {};
                                            acc[ a.Province_State ]["Daily Deaths"] = {};
                                            (this.dates).forEach( (k) => {
                                              acc[ a.Province_State ]["Total Deaths"][k] = Number( a["Total Deaths"][k]);
                                              acc[ a.Province_State ]["Daily Deaths"][k] = Number( a["Daily Deaths"][k]);
                                            });
                                          } else {
                                            acc[ a.Province_State ].Population      += Number( a.Population ); 
                                            (this.dates).forEach( (k) => {
                                              acc[ a.Province_State ]["Total Deaths"][k] += Number( a["Total Deaths"][k]);
                                              acc[ a.Province_State ]["Daily Deaths"][k] += Number( a["Daily Deaths"][k]);
                                            });
                                          }
     }                                     
                                          return acc;
      
                                        }, {});

    return answer;
  }

  organize( ) {

    console.log( "organize begin" );

    let answer = 
      (this.stats.data).map( (x) => {
        let obj = {};
        obj[ this.keys[0] ] = x.shift();    // UID
        obj[ this.keys[1] ] = x.shift();    // iso2
        obj[ this.keys[2] ] = x.shift();    // iso3
        obj[ this.keys[3] ] = x.shift();    // code3
        obj[ this.keys[4] ] = x.shift();    // FIPS
        obj[ this.keys[5] ] = x.shift();    // Admin2
        obj[ this.keys[6] ] = x.shift();    // Province_State
        obj[ this.keys[7] ] = x.shift();    // Country_Region
        obj[ this.keys[8] ] = x.shift();    // Lat
        obj[ this.keys[9] ] = x.shift();    // Long_
        obj[ this.keys[10] ] = x.shift();   // Combined_Key
        obj[ this.keys[11] ] = x.shift();   // Population

        let deltaDeaths = [];
        for( let i = 0; i < x.length; i++) {
          deltaDeaths[i] = ((i<1) ? x[i] : x[i]-x[i-1]);
        }
        
        let obj2 = x.reduce( (acc, a, i) => {
                                    acc[ new Date(this.dates[i]) ] = a;
                                    return acc;
                                  }, {} );
        obj[ "Total Deaths" ] = obj2;

        obj2 = deltaDeaths.reduce( (acc, a, i) => {
                                    acc[ new Date(this.dates[i]) ] = a;
                                    return acc;
                                  }, {} );
        obj[ "Daily Deaths" ] = obj2;

        return obj;  
      });

//    answer.pop();

    console.log( "organize end" );

    return answer;
  }

  getData( url, callback ) {

    var aCallback = this.callbackComplete;
    var dataObject = this;

    Papa.parse(url, {
      download: true,
      complete: function( results ) {
        aCallback( dataObject, results, callback );
      }
    });

    console.log("data.js end");
    
  }
              
}

