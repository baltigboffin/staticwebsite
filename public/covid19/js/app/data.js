var callback = undefined;
var callbackUrl = undefined;

function finished( result ) {

// Remove the rows with errors..
  (result.errors).reverse();  // reverse array so splicing doesn't mess up indexes

  (result.errors).forEach( (a)=>{
    console.log( "Error row:", a.row, "type:", a.type, "code:", a.code, "message:", a.message);
    console.log( "\tRow", a.row, "has been removed." );
    (result.data).splice(a.row,1);
  });
  
// Remove instances where the State field is equal to "Unassigned" values.  
// These values aren't used on several websites.  Not sure what they indicate.

  let tempCount = [];
  (result.data).forEach( (a, i)=>{
    if( a["Admin2"] == "Unassigned" ) {
      tempCount.push( i );
    } 
  });
  tempCount.reverse();
  tempCount.forEach( (i)=> {
    (result.data).splice( i, 1 );
  });

  callback( result, callbackUrl );
}

// transforms dates in string format into values of type Date
var transformHeader = function( x ) {
  let answer = undefined;

/*
  if( (new Date(x)).toString() === "Invalid Date" ) {
    answer = x;
  } else {
    answer = new Date( x );    
  }
*/

  if( (new Date(x)).toString() === "Invalid Date" ) {
    answer = x;
  } else {
    let a = x.split("/");
    answer = (a[0].length === 1 ? "0"+a[0] : a[0]) + "/" + (a[1].length === 1 ? "0"+a[1] : a[1]) + "/20" + (a[2]);
  }

  return answer;
}

// This is the main function that gets the csv data.
export function getData( url, cb ) {
  callbackUrl = url;
  callback = cb;
  Papa.parse( url.url, {
    complete: function( results ) {
      finished( results );
    },
    download: true,
    header: true,
    transformHeader: transformHeader 
  });
}



console.log("data.js end");
    
