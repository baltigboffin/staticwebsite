const http = require('http');
const fs   = require('fs');
const path = require('path');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  console.log( req.url ); 
  if( req.url === "/" ) {
    fs.readFile("./index.html", function (error, pgResp) {

      if( error ) {
        res.writeHead(404);
        res.writeHead('Contents you are looking are Not Found');
      } else {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(pgResp);
      }

      res.end();
    });
  } else if( req.url === "/favicon.ico" ) {
    var filePath = path.join(__dirname, "/favicon.ico");
    var fileStream = fs.createReadStream( filePath );
    res.writeHead(200, {"Content-Type": "image/png"});
    fileStream.pipe( res );
  } else if( req.url.match("\.html$") ) {
    var filePath = path.join(__dirname, req.url);
    var fileStream = fs.createReadStream( filePath );
    res.writeHead(200, {"Content-Type": "text/html"});
    fileStream.pipe( res );
  } else if( req.url.match("\.css$") ) {
    var filePath = path.join(__dirname, req.url);
    var fileStream = fs.createReadStream( filePath );
    res.writeHead(200, {"Content-Type": "text/css"});
    fileStream.pipe( res );
  } else if( req.url.match("\.js$") ) {
    var filePath = path.join(__dirname, req.url);
    var fileStream = fs.createReadStream( filePath );
    res.writeHead(200, {"Content-Type": "application/javascript"});
    fileStream.pipe( res );
  } else if( req.url.match("\.png$")){
    var imagePath = path.join(__dirname, req.url);
    var fileStream = fs.createReadStream( imagePath );
    res.writeHead(200, {"Content-Type": "image/png"});
    fileStream.pipe( res );
  } else {
    res.writeHead(200, {'Content-Type': 'text/html' });
    res.write('<h1>This is my Node.js "page not found" page.</h1><br /><br />');
    res.end();
  }

});


server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

