
import './lib/d3.v5.min.js';
import { TriRow   } from './app/trirow.js';
import { euler202 } from './app/euler202.js';
import { mirroredTriangle } from './app/mirroredTriangle.js';

var numberOfRows = 6; 
d3.select("#levelInput").property("value", numberOfRows) ;

const maximumNumberOfRowsToDisplay = 100;
const xOffset = 100;
const yOffset = 100;
const edgeLength = 50;
const startY = edgeLength * numberOfRows + xOffset;
const startX = startY / 2 + yOffset;

const showALabels = false;
const showBLabels = false;
const showCLabels = true;

var svg = d3.select( "svg" );
var gBigT = svg.select( "g#bigT" );


// n of 5000 takes several minuts. I didn't let it finish
// n of 2000 succeeds relatively quickly
// n of 1998 is 3995 bounces = 666 paths.
var coprimeDict = euler202.createCoprimePairs( 300 );
var createBigT = function( nRows ) {

  var bounces = euler202.bounces_of_n( nRows );
  var vertexAdjustment = euler202.numberOfVertexes( bounces );
  var goodPairs = [];
  var goodIndexes = [];

  for( let x = 0; x <= nRows; x++ ) {
    if( coprimeDict[x] != undefined ) {
      if( coprimeDict[x].includes( nRows+1-x ) ) {

        if( (vertexAdjustment + x) % 3 === 0 ) {
          goodPairs.push( [x, (nRows+1-x)] );
          goodIndexes = goodIndexes.concat( [x, (nRows+1-x)] );
          console.log( x, (nRows+1-x) );
        } 
        
      }
    }
  }

  if( nRows > maximumNumberOfRowsToDisplay ) {
    console.log( "The maximum number of rows that will be displayed is " + maximumNumberOfRowsToDisplay );
    nRows = maximumNumberOfRowsToDisplay;
  } 
  

  console.log( "Number of paths where the laser bounces " + bounces + " times: " +  (goodPairs.length * 2));

  var triangleData = [];
  var textData     = [];
  var rowData      = [];

// build the matrix of triangles

  for( let n=0; n <= nRows; n++ ) {
    let tempRow  =  new TriRow( n, startX, startY, edgeLength);
    triangleData = triangleData.concat(  tempRow.tri );
    textData     = textData.concat( tempRow.txt );
    rowData.push( tempRow );
  }
  
  let row0String = rowData[ 0 ].tri[0].points;
  let row0Array  = row0String.split(" ");
  let row0Point  = [row0Array[0], row0Array[1]];

  let lineData = [];
  let endRow = rowData[ rowData.length - 1 ].tri ;

  for( let i in endRow ) {
    if( goodIndexes.includes( (Number(i)+1)/2 ) ) {
      let iPoints = endRow[i].points.split(" ");
      let mPoint  = row0Point.concat( [iPoints[0], iPoints[1]] );
      lineData.push( mPoint );
    }
  }

// display/update the matrix of triangles

  let littleTs = d3.select("g#bigT")
                   .selectAll("polygon")
                   .data( triangleData );
               
  littleTs.exit().remove();

  littleTs.enter()
    .append( "polygon" )
    .attr( "points", function(d) { return d.points; })
    .attr( "class" , function(d) { return "level"+d.level})
    .attr( "stroke", "black")
    .attr( "fill"  , "#eeeeee")
    .attr( "stroke-width", "1");

// display/update the C Labels

  let cLabels = d3.select("g#bigT")
                  .selectAll("text.vertexLabelC")
                  .data( (showCLabels ? textData : []) );

  cLabels.exit().remove();

  cLabels.enter()
    .append( "text" )
    .attr( "class", "vertexLabelC" )
    .attr( "x", function(d) { return d.c.x; })
    .attr( "y", function(d) { return d.c.y; })
    .text( "C" );

// display/update the A Labels

  let aLabels = d3.select("g#bigT")
                  .selectAll("text.vertexLabelA")
                  .data( (showALabels ? textData : []) );

  aLabels.exit().remove();

  aLabels.enter()
    .append( "text" )
    .attr( "class", "vertexLabelA" )
    .attr( "x", function(d) { return d.a.x; })
    .attr( "y", function(d) { return d.a.y; })
    .text( "A" );

// display/update the B Labels

  let bLabels = d3.select("g#bigT")
                  .selectAll("text.vertexLabelB")
                  .data( (showBLabels ? textData : []) );

  bLabels.exit().remove();

  bLabels.enter()
    .append( "text" )
    .attr( "class", "vertexLabelB" )
    .attr( "x", function(d) { return d.b.x; })
    .attr( "y", function(d) { return d.b.y; })
    .text( "B" );

// remove all red lines

  let gBigLines = d3.select("g#bigT")
                    .selectAll("line")
                    .data([])
                    .exit().remove();

// add red lines for new data

  gBigLines = d3.select("g#bigT")
                    .selectAll("line")
                    .data( lineData )
                    .attr( "x1", function(d) {return d[0];})
                    .attr( "y1", function(d) {return d[1];})
                    .attr( "x2", function(d) {return d[2];})
                    .attr( "y2", function(d) {return d[3];})
                    .attr( "style", "stroke:rgb(255,0,0);stroke-width:2");

  gBigLines.enter()
    .append( "line" )
    .attr( "x1", function(d) {return d[0];})
    .attr( "y1", function(d) {return d[1];})
    .attr( "x2", function(d) {return d[2];})
    .attr( "y2", function(d) {return d[3];})
    .attr( "style", "stroke:rgb(255,0,0);stroke-width:2");
}
// initialize the matrix of triangles

createBigT( numberOfRows );

// initialize the mirrored triangle

var bounces = euler202.bounces_of_n( 6 );

var mTri = new mirroredTriangle( 0, 0, 100 );
let angle0 = 5 * Math.PI / 12
//let angle0 = Math.atan( 303.11/75 );
//let angle0 = Math.PI - Math.atan( 303.11/75 );
let paths = mTri.calcLaser( 0, 0, angle0, 4);
console.log( paths);
let xCenter = 500;
let yCenter = 500;

/*
d3.select("g#smallT")
  .selectAll("line")
  .data( paths )
//  .data(, mTri.returnBorder()])
  .enter()
  .append("line")
  .attr("x1", function(d) { return xCenter + d.x1;} )
  .attr("y1", function(d) { return yCenter - d.y1;} )
  .attr("x2", function(d) { return xCenter + d.x2;} )
  .attr("y2", function(d) { return yCenter - d.y2;} )
  .attr("stroke-width", function(d) { return d.width;} )
  .attr("stroke", function(d) { return d.color; });
*/
/*

let height = Math.sin( PI / 3 ) * 50;
console.log( "height: ", height );
let angleM = 5 * PI / 12;
console.log( "angleM: ", angleM );
let angleMDeg = angleM * 180 / PI;
console.log( "angleM degrees: ", angleMDeg );
let xN = height / Math.tan( angleM );
console.log( "xN    : ", xN );
let yN = Math.tan( angleM ) * xN;
console.log( "yN    : ", yN );

let angleN = Math.tanh( yN / xN );
console.log( "angleN: ", angleN );
let angleNDeg = angleN * 180 / PI;
console.log( "angleN degrees: ", angleNDeg );
*/

// update the number of rows of triangles when the control value is changed

d3.select("#levelInput").on("input", function() { createBigT( Number(this.value) ); } )  ;

// set up the zoom function

var zoom = d3.zoom()
             .on( "zoom", zoomed );

function zoomed() {
  gBigT.attr("transform", d3.event.transform);
}

svg.call( zoom );

