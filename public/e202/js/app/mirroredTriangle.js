export class mirroredTriangle {
//  constructor( newX, newY, h2, height, i, mlr, n ) {
  constructor( xC, yC, h ) {

    let h2 = h / 2;
    let height = Math.sin( Math.PI / 3 ) * h;
    var PI = Math.PI;

    // h - the length of the triangle edge
    // xC - the x position of the C vertex
    // yC - the y position of the C vertex

    // calculate the x,y coordinates of left, right, and middle points of the triangle
/*
    this.left  = { x:(xC-h2), y:(yC-height) };
    this.right = { x:(xC+h2), y:(yC-height) };
    this.mid   = { x: xC    , y: yC         };
*/

    this.left  = { x:(xC-h2), y:(yC+height) };
    this.right = { x:(xC+h2), y:(yC+height) };
    this.mid   = { x: xC    , y: yC         };

    this.leftLimit  = this.left.x;
    this.rightLimit = this.right.x;
    this.upperLimit = this.left.y;
    // this.upperLimit = height;
    this.lowerLimit = this.mid.y;

  }
  
  returnBorder() {
    let ret = [{x1: this.left.x,   y1: this.left.y,   x2: this.right.x,    y2: this.right.y, color: "red",    width: "5"}, 
               {x1: this.right.x,  y1: this.right.y,  x2: this.mid.x,      y2: this.mid.y  , color: "yellow", width: "5"},
               {x1: this.mid.x,    y1: this.mid.y,    x2: this.left.x,     y2: this.left.y , color: "green",  width: "5"} ];
    return ret;
  }

  strikesAB( angle ) {
    let ret = null;
    let PI = Math.PI;
    
    if( angle <= PI && angle >= 0 ) {
      ret = (2*PI) - angle;
    }

    return ret;
  }

  checkAB( xM, yM, angleM ) {
    console.log( "checkAB" ); 
    let xN = this.upperLimit / Math.tan( angleM );
    let yN = Math.tan( angleM ) * xN;

    // let angleN = Math.tanh( yN / xN );
    let angleN = this.strikesAB( angleM );
 
    let xDelta = xN-xM;
    let yDelta = yN-yM;

    let lengthMN = Math.sqrt( Math.pow( xDelta, 2 ) + Math.pow( yDelta, 2 ) );

    return { x: xN, y: yN, angle: angleN, length: lengthMN, side: "AB" };
  }

  strikesBC( angle ) {
    let ret = null;
    let PI = Math.PI;

    if( (angle <=   (PI/3) && angle >= 0    ) ||
        (angle >= 4*(PI/3) && angle <= 2*PI )  ) {
      ret = 8*PI/3 - angle; // 3*PI - angle;
      while( ret > 2*PI ) { ret = ret - 2*PI; }
    }

    return ret;
  }

  checkBC( xM, yM, angleM ) {        
    console.log( "checkBC" ); 
    let xN = 0;
    let PI = Math.PI;

    xN = ( yM - (xM * Math.tan( angleM ) ) );
    xN = xN / ( Math.tan( PI/3 ) - Math.tan( angleM ) );

    let yN = Math.tan( PI/3 ) * xN;
//    let yN = Math.tan( angleM ) * ( 0 - xM ) + yM;


    let angleN = this.strikesBC( angleM );

    let xDelta = xN-xM;
    let yDelta = yN-yM;

    let lengthMN = Math.sqrt( Math.pow( xDelta, 2 ) + Math.pow( yDelta, 2 ) );

    return { x: xN, y: yN, angle: angleN, length: lengthMN, side: "BC" };

  }

  strikesAC( angle ) {
    let ret = null;
    let PI = Math.PI;

    if( Math.PI <= (5*PI/3) && angle >= (2*PI/3) ) {
      ret = 10*PI/3 - angle;
      while( ret > 2*PI ) { ret = ret - 2*PI; }
    }

    return ret;
  }

  checkAC( xM, yM, angleM ) {
    
    let PI = Math.PI;
    let xN = 0;

//    xN = ( yM - (xM * Math.tan( angleM ) ) );
//    xN = xN / ( Math.tan( 2*PI/3 ) - Math.tan( angleM ) );
console.log( "checkAC" );
console.log( "  xM: ", xM, " yM: ", yM, " angleM: ", angleM );
    xN = yM - ( xM * Math.tan( angleM ) )
console.log( "  xN1: ", xN );
    xN = xN / ( Math.tan( 2 * PI / 3 ) - Math.tan( angleM ) );
console.log( "  xN2: ", xN );
    let yN = Math.tan( 2*PI/3 ) * xN;
//    let yN = Math.tan( 2*PI/3 ) * ( xN - xM );
console.log( "  yN:  ", yN );
    let angleN = this.strikesAC( angleM );
console.log( "  angleN: ", angleN );

    let xDelta = xN-xM;
    let yDelta = yN-yM;

    let lengthMN = Math.sqrt( Math.pow( xDelta, 2 ) + Math.pow( yDelta, 2 ) );

    return { x: xN, y: yN, angle: angleN, length: lengthMN, side: "AC" };

  }

  calcLaser( xM, yM, angleM, bounces ) {
    let PI = Math.PI;
    
    console.log( "x: ", xM, " y: ", yM, " angle: ", angleM, " deg: ", (angleM * 180 / PI));

    let mTri0 = this.checkAB( xM, yM, angleM );
    let mTriAB = null;
    let mTriBC = null;
    let mTriAC = null;

    let paths = [];
    paths.push( {x1: xM, y1: yM, x2: mTri0.x, y2: mTri0.y, color:"blue", width:"2"} );

    console.log( mTri0, "deg: ", (mTri0.angle * 180 / PI) );
    for( let bounceCount = bounces; bounceCount > 1; bounceCount-- ) {

      xM = mTri0.x;
      yM = mTri0.y;

      switch( mTri0.side ) {
        case "AB":
          mTriBC = this.checkBC( mTri0.x, mTri0.y, mTri0.angle );
          mTriAC = this.checkAC( mTri0.x, mTri0.y, mTri0.angle );
 
          if( mTriBC.angle != null && mTriAC.angle != null ) {
            mTri0 = ( mTriBC.length < mTriAC.length ? mTriBC : mTriAC );
          } else {
            mTri0 = ( mTriBC.angle != null ? mTriBC : mTriAC );
          }
          break;
        case "BC":
          mTriAB = this.checkAB( mTri0.x, mTri0.y, mTri0.angle );
          mTriAC = this.checkAC( mTri0.x, mTri0.y, mTri0.angle );
      
          if( mTriAB.angle != null && mTriAC.angle != null ) {
            mTri0 = ( mTriAB.length < mTriAC.length ? mTriAB : mTriAC );
//            console.log( "1) mTri0: ", mTri0, ", mTriAB.length: ", mTriAB.length, ", mTriAC.length: ", mTriAC.length );
          } else {
            mTri0 = ( mTriAB.angle != null ? mTriAB : mTriAC );
//            console.log( "2) mTri0: ", mTri0 );
          }
          break;
        case "AC":
          mTriBC = this.checkBC( mTri0.x, mTri0.y, mTri0.angle );
          mTriAB = this.checkAB( mTri0.x, mTri0.y, mTri0.angle );
       
          console.log( " mTriBC.angle: ", mTriBC.angle, ", mTriAB.angle", mTriAB.angle );
          if( mTriBC.angle != null && mTriAB.angle != null ) { 
            
          console.log( " mTriBC.length: ", mTriBC.length, ", mTriAB.length", mTriAB.length );
            mTri0 = ( mTriBC.length < mTriAB.length ? mTriBC : mTriAB );
          } else {
            mTri0 = ( mTriBC.angle != null ? mTriBC : mTriAB );
          }
          break;
        default:
          console.log( "ERROR" );
      }
      paths.push( {x1: xM, y1: yM, x2: mTri0.x, y2: mTri0.y, color:"blue", width:"2"} );

      console.log( mTri0, "deg: ", (mTri0.angle * 180 / PI) );
    }

    console.log( this.returnBorder() );
    return (this.returnBorder()).concat( paths ); 
    // return paths;
  }

  toString2() {
    var ret = String()+ this.mid.x   + " " +
                        this.mid.y   + " " +
                        this.left.x  + " " +
                        this.left.y  + " " +
                        this.right.x + " " +
                        this.right.y; 
    return ret;
  }

}

