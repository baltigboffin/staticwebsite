export var euler202 = {

  n_of_bounces : function( bounces ) {
    return Math.floor( (bounces + 1) / 2 );
  },

  bounces_of_n : function( n ) {
    return (2 * n) - 1;
  },

  numberOfVertexes : function( bounces ) {
    return Math.floor( (bounces + 3) / 2 );
  },

  createCoprimePairs : function( maxLevel ) {
    // https://en.wikipedia.org/wiki/Coprime_integers
    // This function creates a dictionary of coprime pairs (m,n)
    // where the key of the dictionary is m and all paired n's are 
    // stored in an array as the value.

    var aDict = {};
    var root = this;

    var updateDict = function( m0, n0 ) {
      if( aDict[ m0 ] === undefined ) { 
        aDict[ m0 ] = [ n0 ]; 
      } else {
        aDict[ m0 ] = aDict[ m0 ].concat( n0 );
      }
    }

    var callBranches2 = function( m0, n0 ) {
      let branchQueue = [];
      let branch1Queue = [];
      let branch2Queue = [];
      let branch3Queue = [];
      let point0 = [];

      let firstRun = true;

      let m1 = 0;
      let n1 = 0;

      while( firstRun ) {

        m1 = 2*m0 - n0;
        n1 = m0;

        if( m1 <= maxLevel ) {
          updateDict( m1, n1 );
          branchQueue.push( [m1, n1] );
        }

        m1 = 2*m0 + n0;
        n1 = m0;

        if( m1 <= maxLevel ) {
          updateDict( m1, n1 );
          branchQueue.push( [m1, n1] );
        }

        m1 = m0 + 2*n0;
        n1 = n0;

        if( m1 <= maxLevel ) {
          updateDict( m1, n1 );
          branchQueue.push( [m1, n1] );
        }

        if( branchQueue.length > 0 ) {
          point0 = branchQueue.pop();
          m0 = point0[0];
          n0 = point0[1];
          firstRun = true;
        } else {
          firstRun = false;
        }
      }
/*
      while( firstRun || 
             ((branch1Queue.length > 0) &&
              (branch2Queue.length > 0) &&
              (branch3Queue.length > 0) ) ) {
        
        firstRun = false;

        m1 = 2*m0 - n0;
        n1 = m0;

        if( m1 <= maxLevel ) {
          updateDict( m1, n1 );
          branch2Queue.push( [m1,n1] );
          branch3Queue.push( [m1,n1] );
        }

        m1 = 2*m0 + n0;
        n1 = m0;

        if( m1 <= maxLevel ) {
          updateDict( m1, n1 );
          branch1Queue.push( [m1, n1] );
          branch3Queue.push( [m1, n1] );
        }

        m1 = m0 + 2*n0;
        n1 = n0;

        if( m1 <= maxLevel ) {
          updateDict( m1, n1 );
          branch1Queue.push( [m1, n1] );
          branch2Queue.push( [m1, n1] );
        }

        if( branch1Queue.length > 0 ) {
          point0 = branch1Queue.pop();
          m0 = point0[0];
          n0 = point0[1];
        } else if( branch2Queue.length > 0 ) {
          point0 = branch2Queue.pop();
          m0 = point0[0];
          n0 = point0[1];
        } else if( branch3Queue.length > 0 ) {
          point0 = branch3Queue.pop();
          m0 = point0[0];
          n0 = point0[1];
        }

      }
*/
    }

    var callBranches = function( m0, n0 ) {
      branch1( m0, n0 );
      branch2( m0, n0 );
      branch3( m0, n0 );
    }

    var branch1 = function( m, n ) {
      let m1 = 2*m - n;
      let n1 = m;
      if( m1 <= maxLevel ) { 
        updateDict( m1, n1 );
        callBranches( m1, n1 ); 
      }
    }

    var branch2 = function( m, n ) {
      let m1 = 2*m + n;
      let n1 = m;
      if( m1 <= maxLevel ) { 
        updateDict( m1, n1 );
        callBranches( m1, n1 ); 
      }
    }

    var branch3 = function( m, n ) {
      let m1 = m + 2*n;
      let n1 = n;
      if( m1 <= maxLevel ) { 
        updateDict( m1, n1 );
        callBranches( m1, n1 ); 
      }
    }

    if( maxLevel > 2 ) { callBranches2( 2, 1 ); }
    if( maxLevel > 3 ) { callBranches2( 3, 1 ); }
//    if( maxLevel > 2 ) { callBranches( 2, 1 ); }
//    if( maxLevel > 3 ) { callBranches( 3, 1 ); }

    return aDict;
  },

  calcAnswer : function() {
    
  }

}


