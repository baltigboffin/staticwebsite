export class Triangle {
  constructor( newX, newY, h2, height, i, mlr, n ) {
    //
    // h2 - half of the length of the triangle side
    // height - the verticle height of the triangle
    // i - this triangle is the ith triangle on the row of 0..i triangles
    // mlr - indicates if the c vertex is on the middle, left, or right point of the triangle
    // n - the level of the larger matrix of triangles.  0..n levels
    //

    this.n = n;
    
    // calculate the x,y coordinates of left, right, and middle points of the triangle

    if( i%2 === 0 ) { // This option indicates the triangle is pointing down
      this.left  = { x:(newX-h2), y:(newY-height) };
      this.right = { x:(newX+h2), y:(newY-height) };
      this.mid   = { x: newX    , y: newY         };
    } else {          // This option indicates the triangle is pointing up
      this.left  = { x:(newX-h2), y: newY         };
      this.right = { x:(newX+h2), y: newY         };
      this.mid   = { x: newX    , y:(newY-height) };
    }

    // calculate the x,y coordinates of the a,b,c labels assuming c is the middle vertex

    if( i%2 === 0 ) { // This option indicates the triangle is pointing down
      this.a = { x:this.left.x  + 6 , y:this.left.y  + 8 }
      this.b = { x:this.right.x - 11, y:this.right.y + 8 }
      this.c = { x:this.mid.x   - 2 , y:this.mid.y   - 8 }
    } else {          // This option indicates the triangle is pointing up
      this.a = { x:this.left.x  + 6 , y:this.left.y  - 4 }
      this.b = { x:this.right.x - 11, y:this.right.y - 4 }
      this.c = { x:this.mid.x   - 2 , y:this.mid.y   + 12}
    }

    // if the c vertex is the left or right vertex swap the coordinates

    if( mlr === "left" ) {
      let tempLetter = this.b;
      this.b = this.c;
      this.c = this.a;
      this.a = tempLetter;
    } else if( mlr === "right" ) {
      let tempLetter = this.c;
      this.c = this.b;
      this.b = this.a;
      this.a = tempLetter;
    }
  }
  
  toString2() {
    var ret = String()+ this.mid.x   + " " +
                        this.mid.y   + " " +
                        this.left.x  + " " +
                        this.left.y  + " " +
                        this.right.x + " " +
                        this.right.y; 
    return { points: ret, level: this.n }
  }

  textData() {
    return { a:this.a, b:this.b, c:this.c }
  }
}

