import { Triangle } from './triangle.js';


export class TriRow {
  constructor( n, x, y, h ) {
    
    //
    // n - the level of the larger matrix of triangles.  0..n levels
    // x - the starting mid-point (i.e. c vertex) 
    // y - the starting mid-point (i.e. c vertex)
    // h - the length of the triangle edge
    // midleftright - is the c vertex on the left, middle, or right side of the triangle
    //
    
    let triangleCount = 2*n + 1;
    let arrayOfTriangles = [];
    let arrayOfText = [];
    let height = Math.sin( Math.PI / 3 ) * h;
    let h2 = h/2;
    let newY = y - n*height;
    let newX = x - n*h2;
    let midleftright = "";
    
    // determine the starting location of the c vertex depending on the level

    switch( n%3 ) {
      case 0: midleftright = "mid"    ; break;
      case 1: midleftright = "right"   ; break;
      case 2: midleftright = "left"  ; break;
    }

    for( let i=0; i<triangleCount; i++ ) {

      let tempTri = new Triangle( newX, newY, h2, height, i, midleftright, n );
      arrayOfTriangles[ i ] = tempTri.toString2();
      arrayOfText[ i ]      = tempTri.textData();
      newX += h2;

      // determine the location of c on the triangle depending on the position in the row

      switch( midleftright ) {
        case "mid"    : midleftright = "left"; break;
        case "right"  : midleftright = "mid" ; break;
        case "left"   : midleftright = "right"  ; break;
      }
    }
    return { tri: arrayOfTriangles, txt: arrayOfText };
  }
  
}

