Brians-MacBook-Pro:public aplomb76$ sudo certbot certonly -a manual -d www.boffin.app,boffin.app --email hello@boffin.app
Password:
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator manual, Installer None
Cert is due for renewal, auto-renewing...
Renewing an existing certificate
Performing the following challenges:
http-01 challenge for boffin.app
http-01 challenge for www.boffin.app

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
NOTE: The IP of this machine will be publicly logged as having requested this
certificate. If you're running certbot in manual mode on a machine that is not
your server, please ensure you're okay with that.

Are you OK with your IP being logged?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: Y

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Create a file containing just this data:

V9dwfhERBHwuPGObvZ2gZkqZJqo2z6eXZj6iaYkQMSI.bgqIhdxcCMSR3fPWrvaZ0oimWkrY5WIqI1z6ugIE_1Q

And make it available on your web server at this URL:

http://boffin.app/.well-known/acme-challenge/V9dwfhERBHwuPGObvZ2gZkqZJqo2z6eXZj6iaYkQMSI

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Press Enter to Continue

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Create a file containing just this data:

U3l29ng3tzpEhYBVFitHTz06e1jIyR9s3nNQ60XE2xE.bgqIhdxcCMSR3fPWrvaZ0oimWkrY5WIqI1z6ugIE_1Q

And make it available on your web server at this URL:

http://www.boffin.app/.well-known/acme-challenge/U3l29ng3tzpEhYBVFitHTz06e1jIyR9s3nNQ60XE2xE

(This must be set up in addition to the previous challenges; do not remove,
replace, or undo the previous challenge tasks yet.)

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Press Enter to Continue
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/boffin.app/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/boffin.app/privkey.pem
   Your cert will expire on 2020-03-12. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

Brians-MacBook-Pro:public aplomb76$ 

