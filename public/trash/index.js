d3.selectAll("p")
  .data([1,2,3,4,5])
  .enter().append("p")
  .text("This is the font.")
  .style("font-size", function(d) { return (5 * d) + "px"; });